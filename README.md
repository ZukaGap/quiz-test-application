# Quiz-test-application

Coding assignment to help candidates to show their experience in react-native.

## Recommended stack:

- React Native
- Redux Toolkit (optional)
- Firebase (for auth)(optional)
- feel free to install packages you need but with a simple explanation why they

Hello! To get a feeling for your current skills regarding our React Native stack, you've been requested to do this assignment.

## What you need to start project

Feel free to be creative, so there is a simple layout: [Adobe XD](https://drive.google.com/file/d/14Cf_TzeOCBplkUwtY2kTB0E2yte5-3VO/view?usp=sharing) - [view](https://xd.adobe.com/view/b6aba788-948a-4fc8-8a44-015d55716881-e3f8/)

Recommended API [TRIVIA DATABASE](https://opentdb.com/)

Try getting it to work first, then make it better if you have some free time left.

## Getting started

Create a public repository on GitHub/Gitlab

You have a layout on Design file in this repository.

1. Create a <DynamicForm /> component that implements every type of field (Field implementation needs to be in separate components).

**For example:**

```
<EnumComponent/>
<TextComponent/>...
```

2. Do not forget field validation.
3. Use i18n for static words.
